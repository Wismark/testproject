﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DataAcces db = new DataAcces();
            string ServerName = ConfigurationManager.AppSettings["ServerName"];
            string DatabaseName = ConfigurationManager.AppSettings["DatabaseName"];
            string ConnectonString = string.Format($"Data Source={ServerName};Initial Catalog={DatabaseName};Integrated Security=True");
            
            while (true)
            {
                ShowMenu();
                short option = short.Parse(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        {
                            string FilePath = ConfigurationManager.AppSettings["FilePathLoad"];                          
                            //parse csv file
                            StreamReader fs = new StreamReader(FilePath);
                            string str = "";
                            List<Book> books = new List<Book>();
                            fs.ReadLine(); int Count1 = 0, Count2 = 0;
                            while (true)
                            {
                                str = fs.ReadLine();
                                if (str == null) break;
                                Count1++;
                                List<string> record = str.Trim('"').Split(';').AsEnumerable().ToList();
                                Book book = new Book();
                                try // skip bad data record?
                                {
                                    book.Book_Name = record[0];
                                    book.Writer_FirstName = record[1];
                                    book.Writer_LastName = record[2];
                                    book.Publishdate = DateTime.ParseExact(record[3], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                    book.Page_quantity = int.Parse(record[4]);
                                    book.Raiting = byte.Parse(record[5]);
                                    book.Price = decimal.Parse(record[6], CultureInfo.InvariantCulture);
                                    books.Add(book);
                                }
                                catch { Count2++; }
                            }

                            // Create Log
                            Log FileLog = new Log();
                            FileLog.FileName = FilePath.Split('\\').Last();
                            FileLog.FileSize = new FileInfo(FilePath).Length;
                            FileLog.NumOfBrokenRecords = Count2;
                            FileLog.NumOfRecords = Count1;
                            FileLog.UploadDate = DateTime.Now;                         

                            Stopwatch sw = new Stopwatch();
                            sw.Start();
                            // Update data
                            try
                            {
                                db.OpenConnection(ConnectonString);
                                Console.WriteLine("Processing data...");
                                foreach (var book in books)
                                {
                                    db.InsertOrUpdateBook(book);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                Console.WriteLine(ex.InnerException.Message);
                            }
                            sw.Stop();
                            Console.WriteLine("Data was succesfully updated!");
                            FileLog.LoadToDbTime = (sw.ElapsedMilliseconds / 1000.000).ToString() + " sec";
                            db.AddLog(FileLog);
                            db.CloseConnection();
                            Console.WriteLine("press any key...");
                        }
                        break;
                    case 2:
                        {
                            try
                            {
                                XDocument xdoc = new XDocument();
                                string path = ConfigurationManager.AppSettings["FilePathCreate"];
                                byte raiting = byte.Parse(ConfigurationManager.AppSettings["Rating"]);
                                XElement Orders = new XElement("Orders");
                                string date = DateTime.Now.Date.ToString("yyyymmdd");
                                XElement OrderNumber = new XElement("OrderNumber", string.Format("Order_" + date));
                                XElement DocDate = new XElement("DocDate", date);
                                Orders.Add(OrderNumber);
                                Orders.Add(DocDate);

                                XElement OrderElements = new XElement("OrderElements");

                                // Get Books
                                List<Book> books = new List<Book>();
                                db.OpenConnection(ConnectonString);
                                books = db.GetTopXrating(100, raiting);
                                db.CloseConnection();

                                // Create Xml
                                foreach (var book in books)
                                {
                                    XElement OrderElement = new XElement("OrderElement");
                                    XElement Book_Name = new XElement("Book_Name", book.Book_Name);
                                    XElement Writer_FirstName = new XElement("Writer_FirstName", book.Writer_FirstName);
                                    XElement Writer_LastName = new XElement("Writer_LastName", book.Writer_LastName);
                                    OrderElement.Add(Book_Name);
                                    OrderElement.Add(Writer_FirstName);
                                    OrderElement.Add(Writer_LastName);
                                    XElement Book_details = new XElement("Book_details");
                                    XElement Publishdate = new XElement("Publishdate", book.Publishdate.Date.ToString("yyyy-mm-dd"));
                                    XElement PagePrice = new XElement("PagePrice", Math.Round(book.Price / book.Page_quantity, 3));
                                    Book_details.Add(Publishdate);
                                    Book_details.Add(PagePrice);
                                    OrderElement.Add(Book_details);
                                    OrderElements.Add(OrderElement);
                                }
                                Orders.Add(OrderElements);
                                xdoc.Add(Orders);
                                xdoc.Declaration = new XDeclaration("1.0", string.Empty, "yes");
                                xdoc.Save(path + "\\Order_" + DateTime.Now.Date.ToString("yyyymmdd") + ".xml");
                                Console.WriteLine("Document was succesfully created!");
                                Console.WriteLine("press any key...");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                Console.WriteLine(ex.InnerException.Message);
                            }
                            
                        }
                        break;
                    case 3:
                        {  
                            db.OpenConnection(ConnectonString);
                            db.ClearTable("Books");
                            db.ClearTable("LogTable");
                            db.CloseConnection();
                            Console.WriteLine("Done!");
                        }
                        break;
                    default: break;
                };
                Console.ReadKey();
                Console.Clear();

            }
        }
        //foreach (var book in books)
        //    Console.WriteLine(book.Book_Name + " " + book.Writer_FirstName + " " + book.Writer_LastName + " " +
        //        book.Publishdate.Date + " " + book.Page_quantity + " " + book.Raiting + " " + book.Price);

        private static void ShowMenu()
        {
            Console.WriteLine("1. Load file");
            Console.WriteLine("2. Create order");
        }

        public class Book
        {
            //Book_Name;Writer_FirstName;Writer_LastName;Publishdate;Page_quantity;Raiting;Price
            public int Id { get; set; }
            public string Book_Name { get; set; }
            public string Writer_FirstName { get; set; }
            public string Writer_LastName { get; set; }
            public DateTime Publishdate { get; set; }
            public int Page_quantity { get; set; }
            public byte Raiting { get; set; }
            public decimal Price { get; set; }
        }

        public class Log
        {
            public int Id { get; set; }
            public string FileName { get; set; }
            public DateTime UploadDate { get; set; }
            public long FileSize { get; set; }
            public int NumOfRecords { get; set; }
            public int NumOfBrokenRecords { get; set; }
            public string LoadToDbTime { get; set; }
        }

        public class DataAcces
        {
            SqlConnection sqlCn = null;
            public void OpenConnection(string connectionString)
            {
                sqlCn = new SqlConnection();
                sqlCn.ConnectionString = connectionString;
                sqlCn.Open();
            }
            public void CloseConnection()
            {
                sqlCn.Close();
            }
            public void AddLog(Log Filelog)
            {
                SqlCommand cmd = new SqlCommand("Insert Into LogTable (FileName,UploadDate,FileSize," +
                    "NumOfRecords,NumOfBrokenRecords,LoadToDbTime) Values (@FileName,@UploadDate,@FileSize," +
                    "@NumOfRecords,@NumOfBrokenRecords,@LoadToDbTime)", sqlCn);

                cmd.Parameters.AddWithValue("@FileName", Filelog.FileName);
                cmd.Parameters.AddWithValue("@UploadDate", Filelog.UploadDate);
                cmd.Parameters.AddWithValue("@FileSize", Filelog.FileSize);
                cmd.Parameters.AddWithValue("@NumOfRecords", Filelog.NumOfRecords);
                cmd.Parameters.AddWithValue("@NumOfBrokenRecords", Filelog.NumOfBrokenRecords);
                cmd.Parameters.AddWithValue("@LoadToDbTime", Filelog.LoadToDbTime);
                cmd.ExecuteNonQuery();
            }

            public void InsertOrUpdateBook(Book book)
            {
                //check if element exist
                SqlCommand cmdCount = new SqlCommand
                    ("SELECT count(*) from Books WHERE Book_Name = @Book_Name " +
                    "AND Writer_FirstName=@Writer_FirstName " +
                    "AND Writer_LastName=@Writer_LastName", sqlCn);

                cmdCount.Parameters.AddWithValue("@Book_Name", book.Book_Name);
                cmdCount.Parameters.AddWithValue("@Writer_FirstName", book.Writer_FirstName);
                cmdCount.Parameters.AddWithValue("@Writer_LastName", book.Writer_LastName);
                int count = (int)cmdCount.ExecuteScalar();


                string sqlInsert = string.Format("Insert Into Books" +
               "(Book_Name, Writer_FirstName, Writer_LastName, Publishdate, Page_quantity, Raiting, Price) Values" +
               "(@Book_Name, @Writer_FirstName, @Writer_LastName, @Publishdate, @Page_quantity, @Raiting, @Price)");

                string sqlUpdate = string.Format("Update Books Set Book_Name=@Book_Name, Writer_FirstName=@Writer_FirstName," +
                    "Writer_LastName=@Writer_LastName, Publishdate=@Publishdate, Page_quantity=@Page_quantity," +
                    "Raiting=@Raiting, Price=@Price " +
                    "WHERE Book_Name = @Book_Name AND Writer_FirstName=@Writer_FirstName AND Writer_LastName=@Writer_LastName");

                if (count > 0)
                {
                    //Update
                    using (SqlCommand cmd = new SqlCommand(sqlUpdate, sqlCn))
                    {
                        cmd.Parameters.AddWithValue("@Book_Name", book.Book_Name);
                        cmd.Parameters.AddWithValue("@Writer_FirstName", book.Writer_FirstName);
                        cmd.Parameters.AddWithValue("@Writer_LastName", book.Writer_LastName);
                        cmd.Parameters.AddWithValue("@Publishdate", book.Publishdate);
                        cmd.Parameters.AddWithValue("@Page_quantity", book.Page_quantity);
                        cmd.Parameters.AddWithValue("@Raiting", book.Raiting);
                        cmd.Parameters.AddWithValue("@Price", book.Price);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    //Insert
                    using (SqlCommand cmd = new SqlCommand(sqlInsert, sqlCn))
                    {
                        cmd.Parameters.AddWithValue("@Book_Name", book.Book_Name);
                        cmd.Parameters.AddWithValue("@Writer_FirstName", book.Writer_FirstName);
                        cmd.Parameters.AddWithValue("@Writer_LastName", book.Writer_LastName);
                        cmd.Parameters.AddWithValue("@Publishdate", book.Publishdate);
                        cmd.Parameters.AddWithValue("@Page_quantity", book.Page_quantity);
                        cmd.Parameters.AddWithValue("@Raiting", book.Raiting);
                        cmd.Parameters.AddWithValue("@Price", book.Price);
                        cmd.ExecuteNonQuery();
                    }

                }
            }

            public void ClearTable(string tableName)
            {
                SqlCommand cmd = new SqlCommand("TRUNCATE TABLE " + tableName, sqlCn);
                cmd.ExecuteNonQuery();
            }

            public List<Book> GetTopXrating(int number, byte rating)
            {
                List<Book> results = new List<Book>();
                string sql = String.Format("Select TOP {0} * from Books where Raiting>={1}", number.ToString(), rating.ToString());
                SqlCommand cmd = new SqlCommand(sql, sqlCn);

                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    results.Add(new Book
                    {
                        Id = (int)dr["Id"],
                        Book_Name = (string)dr["Book_Name"],
                        Writer_FirstName = (string)dr["Writer_FirstName"],
                        Writer_LastName = (string)dr["Writer_LastName"],
                        Publishdate =(DateTime)dr["Publishdate"],
                        Page_quantity=(int)dr["Page_quantity"],
                        Raiting=(byte)dr["Raiting"],
                        Price=(decimal)dr["Price"]
                    });
                }
                dr.Close();
                return results;
            }
        }
    }
}
