﻿CREATE TABLE [dbo].[Books]
(
	[Id] INT NOT NULL IDENTITY PRIMARY KEY, 
    [Book_Name] NVARCHAR(50) NOT NULL, 
    [Writer_FirstName] NVARCHAR(50) NOT NULL, 
    [Writer_LastName] NVARCHAR(50) NOT NULL, 
    [Publishdate] DATE NOT NULL, 
    [Page_quantity] INT NOT NULL, 
    [Raiting] TINYINT NOT NULL, 
    [Price] DECIMAL(19, 4) NOT NULL
)
