﻿CREATE TABLE [dbo].[LogTable]
(
	[Id] INT NOT NULL IDENTITY PRIMARY KEY, 
    [FileName] NVARCHAR(50) NOT NULL, 
    [UploadDate] DATE NOT NULL, 
    [FileSize] NVARCHAR(50) NOT NULL, 
    [NumOfRecords] INT NOT NULL, 
    [NumOfBrokenRecords] INT NOT NULL, 
    [LoadToDbTime] NVARCHAR(50) NOT NULL
)
